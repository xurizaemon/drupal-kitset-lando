# xurizaemon/drupal-kitset-lando

A kitset asset package to install some files for Drupal + Lando.

## What is it?

Requiring this metapackage will scaffold in some assets for your project:

- .lando.base.yml
- .lando.yml
- Makefile

## Prep for install

Your project's `composer.json` needs to permit this package to scaffold files.

```json
    "extra": {
        "drupal-scaffold": {
            "locations": {
                "web-root": "web/"
            },
            "allowed-packages": [
                "xurizaemon/drupal-kitset-lando"
            ]
        },
    }
```

Since you're in the "private beta" program, you'll need to add this "private repo" VCS to your project's composer.json first:

`composer config repositories.drupal-suggested vcs https://gitlab.com/xurizaemon/drupal-kitset-lando.git`

If this gets blessed, we can release it on Packagist and that won't be required.

## Installation

`composer require --dev xurizaemon/drupal-kitset-lando`
